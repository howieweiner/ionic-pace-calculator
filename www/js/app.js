angular.module('app', ['ionic'])

  // http://snippetrepo.com/snippets/lodash-in-angularjs
  .constant('_', window._)

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .controller('CalcCtrl', function ($scope) {

    $scope.units = [
      {name: 'Mile', value: 'm'},
      {name: 'KM', value: 'km'}
    ];

    $scope.hours = _.range(0, 50);
    $scope.mins = _.range(0, 60);
    $scope.secs = _.range(0, 60);

    $scope.events = [
      {name: 'Marathon', distance: 26.21875, unit: 'm'},
      {name: 'Half Marathon', distance: 13.109375, unit: 'm'},
      {name: '5 mile', distance: 5, unit: 'm'},
      {name: '10 mile', distance: 10, unit: 'm'},
      {name: '5 km', distance: 5, unit: 'km'},
      {name: '10 km', distance: 10, unit: 'km'}
    ];

    $scope.calc = {
      unit: $scope.units[0], // default to mile
      distance: null,
      hours: 0,
      mins: 0,
      secs: 0,
      event: null,
      pace: {
        unit: $scope.units[0]
      }
    };

    $scope.updateDistance = function () {
      if ($scope.calc.event != undefined) {
        // update distance and unit of measure based on selection
        $scope.calc.distance = $scope.calc.event.distance;
        $scope.calc.unit = _.find($scope.units, {'value': $scope.calc.event.unit});

        $scope.updateUnit();
      }
    };

    $scope.updateUnit = function () {
      $scope.calc.pace.unit = $scope.calc.unit;
    };

    $scope.clearEvent = function () {
      $scope.calc.event = null;
    };

    $scope.recalculatePace = function (timeInSecs) {
      var paceInSecs = timeInSecs / $scope.calc.distance;
      var paceMins = parseInt(paceInSecs / 60);
      var paceSecs = (paceInSecs % 60).toFixed(2);
      return paceMins + ' minutes ' + paceSecs + ' seconds';
    };

    $scope.calculatedPace = null;

    $scope.$watchCollection('[calc.distance, calc.hours, calc.mins, calc.secs]',
      function (newValues, oldValues, scope) {
        var distance = newValues[0];
        var hours = newValues[1];
        var mins = newValues[2];
        var secs = newValues[3];

        var timeInSecs = (hours * 3600) + (mins * 60) + secs;
        if(distance != undefined && timeInSecs > 0) {
          $scope.calculatedPace = $scope.recalculatePace(timeInSecs);
        }
        else {
          $scope.calculatedPace = '-';
        }
      });
  });
